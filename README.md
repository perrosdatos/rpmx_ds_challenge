# Rappi fraud classifier

![](img/fraude.jpg)

  ***End point:***  https://us-central1-elite-bedrock-311501.cloudfunctions.net/get_fraud_score

This repository has the resources used to modelling **Fraud** Rappi model:

- exploratory_analysis.ipynb : This notebook was made to identify relationship beetween vars and our target (fraud => 1)
- train.ipynb : This notebook has the machine learning Pipeline used to build a machine learning model to identify fraud propension. Also there is charts used to identify performance and best thresholds.
- evaluation_example.ipynb: This notebook has examples of how to use the productive model and Pipeline.
- using_cloud_function.ipynb: Example of how to use cloud function with Python request.

Also `lambda_code` has the code used to build AWS lambda instance. This directory has requirements.txt which contains libraries requiered to run correctly the code.

```bash
lambda_code/
├── cloud_function.zip # zip added to implement fastly in google cloud - cloud function
├── instrunctions.md # Resume with instructions to upload into google cloud platform
├── main.py # It has the entry of the cloud function flow
├── model_evaluation.py # class used to predict rows
├── models #Latest model trained
│   └── model_2021_04_21.pickle.dat
├── preprocessing.py # class used to build no primary vars
├── requirements.txt # This file contains libs used to run the model
└── validate.py # This class contains code used to validate input integrity


```
# Fast exploration
In order to know tips about modelling features, it was compared categories and bins of the features, all these in order to find linearity or more complex nature in the problem.

## Amount charts

![](img/amount_chart.png)

 One of the first findings was that there is no evidance of linear relation ship, in other words when we divide by buckets our variables, the  precision of the cuts does not get better or does not get worse. For this reason it could be evidence of that a good model to predict fraudsters could be a tree model.

## Places chart and device

![](img/places_chart.png)

The same case in devices if you take in order devide_score, the precision will not improve through the values, it improve but it not has relation with intensity of the var. Also evidence says that the most risk place is which was not identified no-definido).

## Places chart and device

![](img/places_chart.png)

In other hand, the most useful time-features  was moment-day features. This features are artificial variables builded with hour feature. The moment of the day with the lowest risk is over the *madrugada* label.
 

# Model

The final model was builded just using the first main 6 variables. It was builded using XGBoost model and training with a undersampling strategy.

We got a relative improvment of 13 % compared with real precision, It means that we can work first with people how is identified as a fraudster and improve the current global precision. 

## Metrics on test sample (it does not balanced)

![](img/grafico_modelo.png)

### Model metrics

```bash
    ROC 0.5054109741810725
    Recall: 0.1864406779661017
    Precisión: 0.03707865168539326
    Precisión observada(real): 0.032808155699721966
    Mejora en precisión: 1.1301656827270996
```

### Confussion matrix

|Predicted | Actual | 0 | 1 | All|
-----------|--------|---|---|-----
|    0     |        | 4,361|144|4,505|
|    1     |        | 857 |33|890|
|    ALL   |        | 5,218|177| 5,395|



### Feature importance

![](img/feature_importance.png)

The model gives the mayority of the importance to the amount vars. In first place linea_tc and moment-day_tarde it could be influenced as we discuss before because the knoledge of the business process on giving credit cards with higher limit credit and the time who fraudsters chosse to do his frauds.


### Feature explained

Some of the relationships found was related to cashback. I assume that it could be related to the expertise of the client in the product. While clients use the product they get experience and it could prevends fraud. More of this findings are discussed in the presentation.

Another conclusion could be that fraudsters are looking for get cards with the highest credit limits. They looks that they know a lot of how to the linea_tc  is defined. 

Other conclusions maybe are less useful but we could focuse on particula periods of the day and in the expertise of the clients. Represented in the shap chart.

![](img/shap_variables.png)
 

# Cloud function implementation

![](img/cloud_function.png)



One of the best ways to deploy APIs is lambda functions. It has several advantages as availability and cheap aspect.
In the `evaluation_example.ipynb` is an example of how to consume the api available in the endpoint: `using_cloud_function`


All the features were analyzed and it can be found inside ***exploratory_analysis.ipynb***

## Code to consume API
# Libraries


```python
import os, sys, pandas as pd
import requests
import json
```


```python
dataframe = pd.read_csv("ds_challenge_apr2021.csv")
```


```python
row = dataframe.to_dict('records')[100]
```


```python
url = "https://us-central1-elite-bedrock-311501.cloudfunctions.net/get_fraud_score"
headers = {'Content-type': 'application/json', 'Accept': 'text/plain'}

def evaluate(row):
    r = requests.post(url, data=json.dumps(row), headers=headers)
    return (r.json())
```


```python
%%time
score = evaluate(row)
display(row)
print(score)
```


    {'ID_USER': 19,
     'genero': 'F',
     'monto': 705.462887232257,
     'fecha': '2020-01-22',
     'hora': 9,
     'dispositivo': "{'model': 2020, 'device_score': 4, 'os': '.'}",
     'establecimiento': nan,
     'ciudad': nan,
     'tipo_tc': 'Física',
     'linea_tc': 28000,
     'interes_tc': 42,
     'status_txn': 'Aceptada',
     'is_prime': False,
     'dcto': 70.5462887232257,
     'cashback': 6.349165985090313,
     'fraude': True}


    {'ID_USER': 19, 'score': 0.4836121201515198, 'tag': 'no_propenso_fraude'}
    CPU times: user 30 ms, sys: 3.94 ms, total: 33.9 ms
    Wall time: 805 ms



```python

```