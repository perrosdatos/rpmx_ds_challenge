needed_features = ["ID_USER", "dispositivo", "establecimiento", "interes_tc", "linea_tc", "hora", "dcto", "cashback"]
def validate(df):
    for fld in needed_features:
        if fld not in df.columns:
            return False
    return True