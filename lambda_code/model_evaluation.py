import pickle
from preprocessing import evaluate
from validate import validate
import pandas as pd
import xgboost
import sklearn
import numpy as np

THR = 0.53 
file_name = "models/model_2021_04_21.pickle.dat"

model = pickle.load(open(file_name, "rb"))

features_selected = ['device_score_5', 'establecimiento_no-definido', 
                     'interes_tc','linea_tc', 'momentday_tarde', 'dcto', 'cashback']


def eval_row(row):
    
    df_row = pd.DataFrame([row])
    if(not validate(df_row)):
        raise Exception('fields are not completed')
    df_processed = evaluate(df_row)
    y_prob = model.predict_proba(df_processed[features_selected])
    df_row["score"] = y_prob[:,1]
    df_row["tag"] = df_row["score"].apply(lambda el: "propenso_fraude" if el>=0.53 else "no_propenso_fraude")
    return df_row[["ID_USER", "score", "tag"]].to_dict('records')[0]

def eval_rows(rows):
    df_row = pd.DataFrame(rows)
    if(not validate(df_row)):
        raise Exception('fields are not completed')
    df_processed = evaluate(df_row)
    y_prob = model.predict_proba(df_processed[features_selected])
    df_row["score"] = y_prob[:,1]
    df_row["tag"] = df_row["score"].apply(lambda el: "propenso_fraude" if el>=0.53 else "no_propenso_fraude")
    return df_row[["ID_USER", "score", "tag"]].to_dict('records')
