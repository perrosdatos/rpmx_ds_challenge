import pandas as pd
import json 
one_hot_dict = {
        "device_score":[1,2,3,4,5],
        "establecimiento":['Super', 'no-definido', 'MPago', 'Abarrotes', 'Farmacia', 'Restaurante'],
        #"os": ['ANDROID', '.', 'WEB', '%%'],
        #"ciudad": ['Merida', 'Guadalajara', 'Toluca', 'Monterrey', 'no-definido'],
        #"weekday":['work_week', 'end_week'],
        #"monthday": ['lasthalf_month', 'middle_month', 'firsthalf_month', 'end_month'],
        "momentday": ['noche', 'manana', 'madrugada', 'tarde']
    
}
one_hot_fields =  []

def get_onehot_mappings(dataframe):
    global one_hot_fields
    one_hot_fields =  []
    for fld in one_hot_dict.keys():
        for vl in one_hot_dict[fld]:
            dataframe["{}_{}".format(fld,vl)] = dataframe[fld].apply(lambda el: 1 if el == vl else 0 )
            one_hot_fields.append("{}_{}".format(fld,vl))
    return dataframe

def interval_month(day):
    if ((day >= 27) | (day <=3)):
        return "end_month"
    if ((day >= 12) & (day <=18)):
        return "middle_month"
    if (day < 12):
        return "firsthalf_month"
    if (day > 18):
        return "lasthalf_month"
    return "default"

def week_day(day):
    if day >= 4:
        return "end_week"
    
    return "work_week"
def get_time(day):
    if day<=6:
        return "madrugada"
    
    if day<=12:
        return "manana"
    
    if day<=19:
        return "tarde"
    return "noche"

def prepare_data(dataframe):
    numeric_fields = [  "hora", "linea_tc", "interes_tc", "dcto", "cashback"]
    for fld in numeric_fields:
        dataframe[fld] = pd.to_numeric(dataframe[fld]) 
    #dataframe["fecha"] = pd.to_datetime(dataframe["fecha"]).dt.date
    
    #dataframe["weekday"] = pd.to_datetime(dataframe["fecha"]).dt.weekday.apply(week_day)
    #dataframe["monthday"] = pd.to_datetime(dataframe["fecha"]).dt.day.apply(interval_month)
    dataframe["momentday"] = dataframe["hora"].apply(get_time)
    
    # Filling genero field
    #genero_moda = dataframe["genero"].mode()[0]
    #dataframe["genero"] = dataframe["genero"].apply(lambda gn:  None if ((gn is None) | (gn not in ["M", "F"])) else gn)
    #dataframe["genero"] = dataframe["genero"].fillna(genero_moda).apply(lambda el: 1 if el == "M" else 0)
    
    #dataframe["es_fisica"] = dataframe["tipo_tc"].apply(lambda el: 0 if el ==  "Virtual" else 1)
    #dataframe["is_prime"] = dataframe["is_prime"].apply(lambda el: 1 if el ==  True else 0)
    
    dataframe["mobile"] = dataframe["dispositivo"].apply(lambda dc: json.loads(dc.replace("'",'"')))
    dataframe["device_score"] = dataframe["mobile"].apply(lambda el:  el["device_score"])
    #dataframe["os"] = dataframe["mobile"].apply(lambda el:  el["os"])
    
    # Adding target field
    #dataframe["target"] = dataframe["fraude"].apply(lambda el: 1 if el == True else 0)
    
    # Imputting stage
    dataframe["establecimiento"] = dataframe["establecimiento"].fillna("no-definido")
    #dataframe["ciudad"] = dataframe["ciudad"].fillna("no-definido")
    
    return get_onehot_mappings(dataframe)

def evaluate(df):
    return prepare_data(df)
        