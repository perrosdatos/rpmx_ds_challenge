
from model_evaluation import eval_row, eval_rows
import json

def get_score(request):
    """Responds to any HTTP request.
    Args:
        request (flask.Request): HTTP request object.
    Returns:
        The response text or any set of values that can be turned into a
        Response object using
        `make_response <http://flask.pocoo.org/docs/1.0/api/#flask.Flask.make_response>`.
    """
    request_json = request.get_json()
    if request_json is None:
    	return "{'msg':'Error'}"
    

    fields_null = ["ciudad", "establecimiento"]

    for fld in fields_null:
        if fld not in request_json:
            request_json[fld] = None
    

    try:
        return json.dumps(eval_row(request_json))
    except Exception as e:
    	return str(e)
    """
    if request.args and 'message' in request.args:
        return request.args.get('message')
    elif request_json and 'message' in request_json:
        return request_json['message']
    else:
        return f'Hello World!'
    """
    pass